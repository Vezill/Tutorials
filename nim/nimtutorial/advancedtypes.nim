# A new type can be defined within the `type` statement
type
  biggestInt = int64      # Biggest integer type that is available
  biggestFloat = float64  # Biggest float type that is available

# Enums can be defined with the `enum` statement, and are represented by ints internally, starting
# at 0. All comparison operators can be used with enumeration types
type
  Direction = enum
    north, east, south, west

var x = south  # `x` is of type `Direction`; it's value is `south`
echo x         # writes "south" to `stdout`
# You can also qualify an enumeration symbol to avoid ambiguities `Direction.south`
# The `$` operator can convert it to a string, and `ord` can convert it to its underlying value
# You can also assign your own explicit values (required to be in ascending order). Any symbol not
# given a value will be assigned the value of the previous symbol + 1
type
  MyEnum = enum
    a = 2, b = 4, c = 89

# You can also use `range` to define subranges. Ranges allow you to define what range of values
# can be assigned to a variable of that type.
type
  MySubrange = range[0..5]  

# Sets can be defined with `set[T]`, and can be of type int8-16, uint8/byte-uint16, char, enum, or 
# equivalent. This is because they are represented as high performance bit vectors.
# They can be constructed with the `{}` set constructor, `{}` being empty
type
  CharSet = set[char]
var
  d: CharSet
d = {'a'..'z', '0'..'9'}  # this constructs a set that contains the letters a through z, 0 to 9


# Arrays are fixed length containers all of the same type, and are constructed with `[]`
type
  IntArray = array[0..5, int]  # an array that is indexed with 0..5
var
  e: IntArray
e = [1, 2, 3, 4, 5, 6]
for i in low(e)..high(e):
  echo e[i]


block:
  type
    Direction = enum
      north, east, west, south
    BlinkLights = enum
      off, on, slowBlink, mediumBlink, fastBlink
    LevelSetting = array[north..west, BlinkLights]
  var
    level: LevelSetting
  level[north] = on
  level[south] = slowBlink
  level[east] = fastBlink
  echo repr(level)  # --> [on, fastBlink, slowBlink, off]
  echo low(level)   # --> north
  echo len(level)   # --> 4
  echo high(level)  # --> west

  # Multidimensional arrays
  type
    LightTower = array[1..10, LevelSetting]
  var
    tower: LightTower
  tower[1][north] = slowBlink
  tower[1][east] = mediumBlink
  echo len(tower)     # --> 10
  echo len(tower[1])  # --> 4
  echo repr(tower)    # --> [[slowBlink, mediumBlink, ...
  # the following lines don't compile due to type mismatch errors
  # tower[north][east] = on
  # tower[0][1] = on
  
  # Another way to fully define LightTower would be:
  block alt_lighttower:
    type
      LightTower = array[1..10, array[north..west, BlinkLights]]

  # It's quite common to define arrays starting with index 0, so there's shorthand for this:
  type
    IntArray = array[0..5, int]  # An array that is indexed 0..5
    QuickArray = array[6, int]   # An array that is indexed 0..5
  var
    x: IntArray
    y: QuickArray
  x = [1, 2, 3, 4, 5, 6]
  y = x  # copies the array
  for i in low(x)..high(x):
    echo x[i], y[i]


# Sequences are similar to arrays but are of a dynamic length that can change during runtime. They
# are always allocated on the heap and garbage collected. Sequences can be constructed with the `[]`
# array constructor and the `@` toSeq operator, or using the `newSeq` procedure.
block seqs:
  var x: seq[int]
  x = @[1, 2, 3, 4, 5, 6]  # the `@` turns the array into a sequence

  # Sequences are always initialized with `nil`, but most seq operators cannot deal with nil, so it
  # is recommended to initialize empty sequences with `@[]`, but keep in mind this will create an
  # object on the heap.
  # The `for` statement can be used with a sequence with one or two variables. When using two, the
  # for statement is actually iterating over the results given from `items()` iterator.
  for value in @[3, 4, 5]:
    echo value

  for i, value in @[3, 4, 5]:
    echo "index: ", $i, ", value: ", $value

  
block openarrays:
  # Note: OpenArrays can only be used for parameters
  # An open array allows you to specify a parameter for a procedure that will accept an array-type 
  # of any size.
  var
    fruits: seq[string]         # Reference to a sequence of strings that is initialized with 'nil'
    capitals: array[3, string]  # array of strings with a fixed size

  fruits = @[]  # creates an empty sequence on the heap that will be referenced by 'fruits'
  capitals = ["New York", "London", "Berlin"]  # array 'capitals' can only have three elements

  fruits.add("Banada")  # Sequence 'fruits' is dynamically expandable during runtime
  fruits.add("Mango")

  proc openArraySize(oa: openArray[string]): int =
    oa.len

  assert openArraySize(fruits) == 2    # procedure accepts a sequence as parameter
  assert openArraySize(capitals) == 3  # but also an array type


block varargsparams:
  # `varargs` parameter is like an openArray parameter except it is also a means to implement 
  # passing a variable number of arguments to a procedure. The compiler will convert the list of
  # arguments to an array automatically
  proc myWriteln(f: File, a: varargs[string]) =
    for s in items(a):
      write(f, s)

    write(f, "\n")


  myWriteln(stdout, "abc", "def", "xyz")
  # Automatically transformed by the compiler to:
  myWriteln(stdout, ["abc", "dec", "xyz"])

  # The transformation is only done if the varargs is the last parameter in the procedure header.
  # It is also possible to perform type conversions in this context:
  proc myWriteln2(f: File, a: varargs[string, `$`]) =
    for s in items(a):
      write(f, s)
    write(f, "\n")

  myWriteln2(stdout, 123, "abc", 4.0)
  # Automatically transformed to:
  myWriteln2(stdout, [$123, $"abc", $4.0])


block slices:
  # Slices look similar to subranges in syntax but are used in a different context. A slice is just
  # an object of type Slice which contains two bounds, a and b. By itself a slice is not very
  # usefull, but other collection types define operators which accept Slice objects to define
  # ranges
  var
    a = "Nim is a programming language"
    b = "Slices are useless."

  echo a[7..12]  # --> 'a prog'
  b[11..^2] = "useful"
  echo b  # --> 'Slices are useful.'


block tuples:
  # A tuple defines various fields and an order of the fields. The constructor `()` can be used to
  # construct tuples. The order of fields in the constructor must match the order in the tuples
  # definitions. There are two ways to access a tuple, either t.fieldName, or [i] for the ith field
  type
    Person = tuple[name: string, age: int]  # Type representing a person 

  var
    person: Person

  person = (name: "Peter", age: 30)
  # The same, but less readable:
  person = ("Peter", 30)

  echo person.name
  echo person.age

  echo person[0]
  echo person[1]

  # You don't need to declare tuples in a separate type section
  var building: tuple[street: string, number: int]
  building = ("Rue del Percebe", 13)
  echo building.street

  # You cannot assign two different types of tuples to each other
  # person = building

  # The following works because the field names and types are the same
  var teacher: tuple[name: string, age: int] = ("Mark", 42)
  person = teacher

  # Tuples can also be unpacked during assignment
  let (name, age) = person
  echo name, " ", age
  echo person.name, " ", person.age


block ref_and_pointers:
  # Nim contains both traced (refs) and untraced (pointers) references. Traced references point to
  # objects in the garbage collected heap, untraced point to manually allocated objects or objects
  # elsewhere in memory, thus untraced references are *unsafe*.
  # Traced references are declared with the `ref` keyword; untraced references are declared with the
  # `ptr` keyword. 
  # The empty `[]` subscript notation can be used to *defer* to a reference, meaning to retrieve the
  # item the reference points to. The `.` accessor and `[]` index operator perform implicit
  # dereferencing operations for reference types
  type
    Node = ref NodeObj
    NodeObj = object
      le, ri: Node
      data: int

  var
    n: Node
  
  new(n)
  n.data = 9
  # No need to write `n[].data`; in fact, `n[].data` is highly discouraged
  # The built in procedure `new` must be used to allocate a traced object, otherwise it has a value
  # of `nil`. To deal with untraced memory, the procedures `alloc`, `dealloc` and `realloc` can be 
  # used.


block procedural_type:
  # A procedural type is a somewhat abstract pointer to a procedure. `nil` is an allowed value for
  # a variable of a procedural type. 
  proc echoItem(x: int) = echo x

  proc forEach(action: proc (x: int)) =
    const data = [2, 3, 5, 7, 11]
    for d in items(data):
      action(d)

  forEach(echoItem)
