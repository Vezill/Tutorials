# You can define objects in nim with the `object` keyword
# Nim recommends that an empty constructor `T()` only be used internally, and that a proc be defined
# to construct the object
# Objects also have access to their type at runtime, and there is an `of` operator that can be used
# to check the objects type
# Object inheritance is done with the `object of` syntax, although multiple inheritance is not 
# supported. If an object has no suitable ancestor, `RootObj` can be used, but this is only a 
# convention. Objects that have no ancestor are implicity `final`.
# Composition (has-a relation) is often preferable to inheritance (is-a relation) for simple code
# reuse. Since objects are value types in nim, composition is as effecient as inheritance.
block objects:
  type
    Person = ref object of RootObj
      name*: string  # the `*` means that `name` is accessible from other modules
      age: int       # no `*` means that the field is hidden from other modules

    Student = ref object of Person  # Student inherits from Person
      id: int        # with an extra `id` field

  var 
    student: Student
    person: Person
  
  assert(student of Student)
  # Object construction
  student = Student(name: "Anton", age: 5, id: 2)
  echo student[]
