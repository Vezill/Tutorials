# The `$` operator 'stringifies' any basic type into a string, which can then be printed to the 
# console with the `echo` proc. That being said, with advanced types and user created types it won't
# work until you define the operator itself. If you want to debug the current value of a complex
# type without defining `$` you can use the `repr` proc.
var
  myBool = true
  myChar = 'n'
  myString = "nim"
  myInt = 42
  myFloat = 3.14

echo myBool, ":", repr(myBool)
# --> true:true
echo myChar, ":", repr(myChar)
# --> n:'n'
echo myString, ":", repr(myString)
# --> nim:0x10fa8c050"nim"
echo myInt, ":", repr(myInt)
# --> 42:42
echo myFloat, ":", repr(myFloat)
# --> 3.1400000000000001e+00:3.1400000000000001e+00
