echo "Counting to ten: "
for i in countup(1, 10):
  echo i

# An `iterator` can be defined using the `yield` statement to create custom iterators
iterator countup(a, b: int): int =
  var res = a
  while res <= b:
    yield res
    inc(res)

# Iterators are similar to procedures, with the following changes:
# - can only be called from for loops
# - cannot contain a `return` statement (and similarly, procs can't contain a `yield` statement)
# - iterators do not have a `result` variable
# - iterators do not support recursion
# - iterators cannot be forward declared
