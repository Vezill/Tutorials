block:
  var x, y: int  # declares x and y to have type `int`

# Indentation can be used after `var` to list whole sections of variables
block:
  var
    x, y: int
    # A comment can occur here too
    a, b, c: string

block:
  var x = "abc"  # introduces a new variable `x` and assigns a value to it
  x = "xyz"      # assigns a new value to `x`

block:
  var x, y = 3   # assigns 3 to the variables `x` and `y`
  echo "x ", x   # outputs "x 3"
  echo "y ", y   # outputs "y 3"
  x = 42         # changes `x` to 42 without changing `y`
  echo "x ", x   # outputs "x 42"
  echo "y ", y   # outputs "y 3"

block:
  const x = "abc"  # the constant x contains the string "abc"

block:
  const
    x = 1
    # A comment can occur here too
    y = 2
    z = y + 5  # computations are possible

block:
  let x = "abc"  # introduces a new variable `x` and binds a value to it
  x = "xyz"      # illegal: assignment to `x`

# The difference between let and const are:
# - `let` introduces a variable that can not be re-assigned
# - `const` means "enforce compile time evaluation and put into a data section"

block:
  const input = readLine(stdin)  # Error: constant expression expected
  let input = readLine(stdin)    # works 
