# Nim supports a module system where a file can be considered a module
# Only top level symbols marked with an `*` are exported
var 
  x*, y: int

proc `*`*(a, b: seq[int]): seq[int] =
  # Allocate a new seq
  newSeq(result, len(a))
  # Multiply two int seqs
  for i in 0..len(a)-1: result[i] = a[i] * b[i]


# This is similar to pythons `if __name__ == '__main__':` line
when isMainModule:
  # test the new `*` operator for sequences
  assert(@[1, 2, 3] * @[1, 2, 3] == @[1, 4, 9])


block moduleA:
  type
    T1* = int     # Module A exports the type `T1`
  import moduleB  # the compiler starts parsing B

  proc main() =
    var i = p(3)  # Works because B has been parsed completely here

  main()
  var x*: string
  proc y*(a: int): string = $a

block moduleB:
  import moduleA  # A is not parsed here! Only the already known symbols of A are imported

  proc p*(x: moduleA.T1): moduleA.T1 =
    # This works because the compiler has already added T1 to A's interface symbol table
    result = x + 1

  var x*: int
  proc y*(a: string) string = $a

block moduleC:
  import moduleA, moduleB
  write(stdout, x)  # Error: x is ambiguous
  write(stdout, moduleA.x)  # Ok: qualifier is used

  var x = 4
  write(stdout, x)  # not abiguous: uses module C's x
  # You only need to qualify a module symbol if it's ambiguous, otherwise it's assumed 
  # This does not apply to procs or iters though, they will auto detect which to use based on type
  write(stdout, y(3))  # No error: A.y is called
  write(stdout, x(""))  # No error: B.y is called

  proc y*(a: int): string = nil
  write(stdout, y(3))  # Error: ambiguous, which `y` is to be called


# You can also exclude symboms with the `except` qualifier
from myModule except y

# As well as just specific symbols
from myModule import x, y, z
x()
# Or force namespace qualifications by importing nil
from myModule import nil
myModule.x() # works
x() # compile error 

# You can also create a module alias with `as`
from myModule as m import nil
m.x()


# The `include` statement just includes all the contents of another file, instead of importing the
# accessable symbols. It can be useful to split up very large modules into multiple files 
include fileA, fileB, fileC
