block if_statement:
  let name = readLine(stdin)
  if name == "":
    echo "Poor soul, you lost your name?"
  elif name == "name":
    echo "Very funny, your name is name."
  else:
    echo "Hi, ", name, "!"


block case_statement:
  let name = readLine(stdin)
  case name
  of "":
    echo "Poor soul, you lost your name?"
  of "name":
    echo "Very funny, your name is name."
  of "Dave", "Frank":
    echo "Cool name!"
  else:
    echo "Hi, ", name, "!"


from strutils import parseInt
block case_ranges:
  echo "A number please: "
  let n = parseInt(readLine(stdin))
  case n
  of 0..2, 4..7: echo "The number is in the set: {0, 1, 2, 4, 5, 6, 7}"
  of 3, 8: echo "The number is 3 or 8"
  # You *must* cover every possible value
  else: discard  # aka a "do nothing" statement


block while_statement:
  echo "What's your name? "
  var name = readLine(stdin)
  while name == "":
    echo "Please tell me your name: "
    name = readLine(stdin)
    # no ``var``, because we do not declare a new variable here


block for_statement:
  echo "Counting to ten: "
  for i in countup(1, 10):
    echo i

  # The above is the same as:
  echo "Counting to ten: "
  var i = 1
  while i < 10:
    echo i
    inc(i)

  # Since counting occurs so often, nim has a range operator (`..`)
  for i in 1..10:
    discard "Hello, world!"
  # There are shortcuts as well, `..<` and `..^` to count to one less than the higher index
  for i in 0..<10:
    discard "Hello, world!"
    
  var s = "some string"
  for i in 0..<s.len:
    echo i

  # Other useful iterators include:
  # `items` and `mitems` for immutable and mutable elements respectively
  # `pairs` and `mpairs` for immutable and mutable element and index number
  for index, item in ["a", "b"].pairs:
    echo item, " at index ", index


block scopes:
  # Control flow statements open a new scope
  while false:
    var x = "Hi"
  # echo x  # does not work
  # Can create a scope manually using the `block` statement, with an optional label
  block myBlock:
    var x = "Hi"
  # echo x  # does not work


block break_statement:
  # Scopes can be exited early with the `break` statement, leaving he inner most block
  block myBlock:
    echo "Entering block"
    while true:
      echo "looping"
      break  # leaves the loop, but not the block
    echo "Still in the block"

  block myBlock2:
    echo "Entering block"
    while true:
      echo "looping"
      break myBlock2  # leaves the block and the loop, by providing the label
    echo "Still in the block"


block continue_statement:
  # `continue` immediately starts the next iteration
  while true:
    let x = readLine(stdin)
    if x == "": continue
    echo x


block when_statement:
  # The `when` statement is similar to `if`, with the following differences:
  # - each condition must be a constant expression since it is evaluated by the compiler
  # - the statements within a branch **do not** open a new scope
  # - the compiler checks the semantics and produces code *only* for the statements that belong to
  #   the first condition that evaluates to true
  # The `when` statement is useful for writting code similar to `#ifdef` statements in C
  # Extra note: When looking to comment large pieces of code, you can use `when false:` then real
  # comments, this way nesting is possible
  when system.hostOS == "windows":
    echo "Running on windows!"
  elif system.hostOS == "linux":
    echo "Running on linux!"
  elif system.hostOS == "macosx":
    echo "The fuck are you doing on a mac?"
  else:
    echo "Unknown operating system"


block indentation:
  var x, y = true
  # No indentation required for single assignment statement
  if x: x = false

  # Indentation needed for nesting if statement
  if x:
    if y:
      y = false
    else:
      y = true

  # Indentation needed, because two statements follow the condition
  if x:
    x = false
    y = false

  # With parens and semicolons you can use statements where only an expression is allowed
  const fac4 = (var n = 1; for i in 1..4: n *= i; n)
