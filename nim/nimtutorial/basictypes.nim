# Booleans are defined with `bool`
# Booleans support the operators `not`, `and`, `or`, `xor`, `<`, `<=`, `>`, `>=`, `!=`, `==`
while p != nil and p.name != "xyz":
  # p.name is not evaluated if p == nil
  p = p.next

# Characters are defined with `char` and single quotes`'`
# Characters support the operators `==`, `<`, `<=`, `>`, `>=`, `$`
# You can convert a character to a string using the `$` operator, and to find the ordinal value use
# the `ord` proc, and `chr` proc to find the char of an integer value

# Strings are mutable in nim, and appending is implemented quite efficiently. They are 
# zero-terminated and have a length field `len` which never counts the terminator. Accessing the 
# terminator is not considered an error, and often leads to simplier code.
if s[i] == 'a' and s[i+1] == 'b':
  # No need to check whether ``i < len(s)``!
  discard
# The `=` operator *copies* the string, and you can use `&` to concat strings, and `+` to append to
# a string.
# They can also be accessed like a list, returning a char value at the given index. Strings are 
# initialized with `nil`, but most operations will not deal with this well, so it's best to
# initialize them to `""` an empty string (note: this will create the value on the heap, so there
# is a trade off)

# Integers are defined with `int`, supports the following: int int8, int16, int32, int64, uint,
# uint8, uint16, uint32, uint64 using a type suffic to specify `'i64` for int64
# The following operators are supported: `+`, `-`, `*`, `div`, `mod`, `<`, `<=`, `==`, `!=`, `>`,
# `>=`. `and`, `or`, `xor`, `not` are also supported as bitwise operators, as well as `shl` and
# `shr` for bitshifting left and right respectively.
let
  x = 0      # x is of type ``int``
  y = 0'i8   # y is of type ``int8``
  z = 0'i64  # z is of type ``int64``
  u = 0'u    # u is of type ``uint``

# Floats are defined with `float`, and supports float, float32 and float64. Floats are 64bit by
# default
# Floats can have a suffix in the same way as ints to specify the type
# Floats support the following operators: `+`, `-`, `*`, `/`, `<`, `<=`, `==`, `!=`, `>`, `>=`
# You can convert between floats and ints using `toInt` and `toFloat`
var
  a = 0.0      # a is of type ``float``
  b = 0.0'f32  # b is of type ``float32``
  c = 0.0'f64  # c is of type ``float64``


# Type coversion is performed using the type as a function
var
  i: int32 = 1.int32   # Same as calling int32(i)
  j: int8 = int8('a')  # 'a' == 97'i8
  k: float = 2.5       # int(2.5) rounds down to 2
  sum: int = int(i) + int(j) + int(k)  # sum == 100
