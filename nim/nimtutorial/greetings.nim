# This is a comment
# Use `nim c -r greetings.nim` to compile in debug mode
# and `nim c -d:release greetings.nim` to compile in release mode without debugging symbols
echo "What's your name? "
var name: string = readLine(stdin)
echo "Hi, ", name, "!"
