# Nim uses `procedures` to represent functions and methods
proc yes(question: string): bool =
  echo question, " (y/n)"
  while true:
    case readLine(stdin)
    of "y", "Y", "yes", "Yes": return true
    of "n", "N", "no", "No": return false
    else: echo "Please be clear: yes or no"


if yes("Should I delete all your important files?"):
  echo "I'm sorry Dave, I'm afraid I can't do that."
else:
  echo "I think you know what the problem is just as well as I do."


# Procedures automatically initialize a `result` variable to the return type of the proc and it
# is returned automatically when the method returns
proc sumTillNegative(x: varargs[int]): int =
  for i in x:
    if i < 0:
      return
    result += i


echo sumTillNegative()  # echos 0
echo sumTillNegative(3, 4, 5)  # echos 12
echo sumTillNegative(3, 4, -1, 6)  # echos 7


# Params are constant by default. One way to get a mutable version of it is to shadow it with `var`
proc printSeq(s: seq, nprinted: int = -1) =
  var nprinted = if nprinted == -1: s.len else: min(nprinted, s.len)
  for i in 0..<nprinted:
    echo s[i]

# If the procedure needs to modify the argument for the caller, it can be declared as a `var` param
proc divmod(a, b: int; res, remainder: var int) =
  res = a div b        # integer division
  remainder = a mod b  # integer modulo operation

var
  x, y: int
divmod(8, 5, x, y)  # modifies x and y
echo x
echo y


# To call a procedure that returns a value just for its side effects and ignoring its return value,
# a `discard` statement **must** be used. Nim does not allow silently throwing away return values
discard yes("May I ask a pointless question?")

# That being said, a proc can be declared with the `discardable` pragma to ingore it's return
proc p(x, y: int): int {.discardable.} =
  return x + y

p(3, 4)  # now valid


# Named arguments can be used to specifically show which value belongs to which param
proc createWindow(x, y, width, height: int; title: string; show: bool): Window =
  ...

var w = createWindow(show = true, title = "My Application", x = 0, y = 0, height = 600, width = 800)
# While you can mix named and unnamed arguments, it is not recommended for readability reasons 
var w = createWindow(0, 0, title = "My Application", height = 600, width = 800, true)


# Overloading procedures are also supported
proc toString(x: int): string = ...
proc toString(x: bool): string =
  if x: result = "true"
  else: result = "false"

echo toString(13)    # calls the toString(x: int) proc
echo toString(true)  # calls the toString(x: bool) proc


# Operators can be defined by placing them in backticks `\``
proc `$`(x: myDataType): string = ...
# Now the $ operator also works with myDataType, overloading resolution ensures that $ works for 
# built-in types just like before
# The `\`` notation cal also be used to call an operator just like any other procedure
if `==`( `+`(3, 4), 7): echo "True"


# Every variable, procedure, etc needs to be declared before it can be used. You can use forward
# declarations to accomplish this if needed. That being said, mutually recursive procedures do not
# support this
proc even(n: int): bool

proc odd(n: int): bool =
  assert(n >= 0)  # Makes sure we don't run into negative recursion
  if n == 0: false
  else: n == 1 or even(n-1)

proc even(n: int): bool =
  assert(n >= 0)  # Makes sure we don't run into negative recursion
  if n == 1: false
  else: n == 0 or odd(n-1)
