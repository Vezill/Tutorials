# A comment
var myVariable: int  ## A document comment
#[
You can have any Nim code text commented
out inside this with no indentation restrictions.
    yes("May I ask a pointless question?")
    #[
        Note: these can be nested!!
    ]#
]#

discard """ You can have any Nim code test commented
out inside this with no indentation restrictions.
    yes("May I ask a pointless question?") """
