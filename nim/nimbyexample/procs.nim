proc fibonacci(n: int): int =
    if n < 2:
        result = n
    else:
        # Procs have a uniform function call syntax, so both forms here work the same
        result = fibonacci(n - 1) + (n - 2).fibonacci

# Encapsulation is supported by annotating a procedure with `*`
# Module 1
proc foo*(): int = 2
proc bar(): int = 3

# Module 2
echo foo()  # Valid
echo bar()  # Will not compile

# Can force a procedure will not have side effects
proc sum(x, y: int): int {. noSideEffect .} =
    x + y

proc minus(x, y: int): int {. noSideEffect .} =
    echo x  # error: 'minus' can have side effects
    x - y

# You can encase a symbol with `s to create operators
proc `$`(a: array[2, array[2, int]]): string =
    result = ""
    for v in a:
        for vx in v:
            result.add($vx & ", ")
        result.add("\n")

echo([[1, 2], [3, 4]])  # See varargs for how echo works

proc `^&*^@%`(a, b: string): string =
    ## A confusingly named useless operator
    result = a[0] & b[high(b)]

assert("foo" ^&*^@% "bar" == "fr")

let zero = ""
proc `+`(a, b: string): string =
    a & b

proc `*`[T](a: T, b: int): T =
    result = zero
    for i in 0..b:
        result = result + a  # Calls `+` from line 3

assert("a" * 10 == "aaaaaaaaaa")
