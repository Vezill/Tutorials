# Result is automatically initialized as var result: ReturnType and is automatically returned
proc getAlphabet(): string =
    result = ""
    for letter in 'a'..'z':
        result.add(letter)

proc unexpected(): int =
    var result = 5
    result += 5

echo unexpected()  # Prints 0, not 10