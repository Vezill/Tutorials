# Case statement, every possible case **must** be covered
case "charlie":
    of "alfa":
        echo "A"
    of "bravo":
        echo "B"
    of "charlie":
        echo "C"
    else:
        echo "Unreconized letter"

case 'h':
    of 'a', 'e', 'i', 'o', 'u':
        echo "Vowel"
    of '\127'..'\255':
        echo "Unknown"
    else:
        echo "Constant"

proc positiveOrNegative(num: int): string =
    result = case num:
        of low(int).. -1:
            "negative"
        of 0:
            "zero"
        of 1..high(int):
            "positive"
        else:
            "imposible"

echo positiveOrNegative(-1)

# Possible values are narrowed, so an else statement is not always required
let x = 2
case (x and 0b11) + 7:
    of 7: echo "A"
    of 8: echo "B"
    of 9: echo "C"
    of 10: echo "D"