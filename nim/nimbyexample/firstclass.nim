
import sequtils


let powersOfTwo = @[1, 2, 4, 8, 16, 32, 64, 128, 256]

echo powersOfTwo.filter do (x: int) -> bool: x > 32
echo powersOfTwo.filter(proc (x: int): bool = x > 32)

# Procedures can be annotated with `{. procvar .} to allow them to be passed as functions
proc greaterThan32(x: int): bool {.procvar.} = x > 32
echo powersOfTwo.filter(greaterThan32)
