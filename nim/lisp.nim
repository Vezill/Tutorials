import os, parseutils, rationals, sequtils, tables, unicode
import strutils except toLower, toUpper


# Number #
type
  Number* = Rational[int]

template number*(i: float): Number = toRational(i, high(int32))
template number*[T](i: T): Number = toRational(i)


# Error #
type
  Error* = object  ## Error object
    s*: string


template error*(str: string): Error =
  Error(s: str)


# Env, Builtin, Fun, Atom #
type
  Env = ref EnvObj  ## Environment for ``eval()``
  EnvObj = object
    table: TableRef[string, Atom]  ## Environment table
    outer: Env                     ## Outer (parent) environment

  Builtin* = proc(args: openArray[Atom]): Atom {.cdecl.}  ## Built-in proc type

  Fun* = object  ## Function object
    args: seq[string]  ## Function arguments (for user-defined functions)
    body: seq[Atom]    ## Function body (for user-defined functions)
    builtin: Builtin   ## Function reference (for built-in functions)
    env: Env           ## Function environment

  AtomKind* = enum
    aList, aNumber, aSymbol, aBool, aFun, aError

  Atom* = object                  ## Atom type
    case kind*: AtomKind          ## Variant of
    of aList:   list*: seq[Atom]  ## List of atoms
    of aNumber: n*: Number        ## Number
    of aSymbol: s*: string        ## Symbol
    of aBool:   b*: bool          ## Boolean
    of aFun:    f*: Fun           ## Function
    of aError:  e*: Error         ## Error


# Atom #
template atom*(): Atom = Atom(kind: aList, list: @[])
template atom*(val: seq[Atom]): Atom = Atom(kind: aList, list: val)
template atom*(val: openArray[Atom]): Atom = Atom(kind: aList, list: @val)
template atom*(val: Number): Atom = Atom(kind: aNumber, n: val)
template atom*(val: string): Atom = Atom(kind: aSymbol, s: val)
template atom*(val: bool): Atom = Atom(kind: aBool, b: val)
proc atom*(val: Fun): Atom = Atom(kind: aFun, f: val)
proc atom*(err: Error): Atom = Atom(kind: aError, e: err)
proc atom*(builtin: Builtin): Atom =
  atom(Fun(args: @[], body: @[], builtin: builtin))
proc atom*(args: seq[string], body: seq[Atom], env: Env): Atom =
  atom(Fun(args: args, body: body, env: env))


proc `not`*(a: Atom): Atom =
  return case a.kind:
    of aBool:
      atom(not a.b)
    else:
      atom error "Not a boolean value"


proc `==`*(a, b: Atom): bool =
  if a.kind != b.kind:
    return false
  else:
    case a.kind:
    of aList:
      if a.list.len != b.list.len:
        return false

      else:
        for i in 0..<a.list.len:
          if a.list[i] != b.list[i]:
            return false
        return true
    of aNumber:
      return a.n == b.n
    of aSymbol:
      return cmp(a.s, b.s) == 0
    of aBool:
      return a.b == b.b
    of aFun:
      return a.f == b.f
    of aError:
      return cmp(a.e.s, b.e.s) == 0


# Symbol Procs #
proc is_comment*(a: Atom): bool =
  ## ``Return`` `true` if atom is a comment (starts with `;`) or `false` otherwise
  return case a.kind:
    of aSymbol: a.s[0] == ';'
    else: false


proc is_quoted*(a: Atom): bool =
  ## ``Return`` `true` if atom is quoted (starts with `'`) or `false` otherwise
  return case a.kind:
    of aSymbol: a.s[0] == '\''
    else: false


proc is_string*(a: Atom): bool =
  ## ``Return`` `true` if atom is string (bound in quotes) or `false` otherwise
  return case a.kind:
    of aSymbol: (a.s[0] == '\"') and (a.s[^1] == '\"')
    else: false


proc `$`*(obj: Atom): string  # forward declaration
proc str_strip*(a: Atom): string =
  ## Strip quotes from around the string
  if a.is_string:
    a.s[1..^2]
  else:
    $a


proc is_valid_id*(a: Atom): bool =
  ## ``Return`` `true` if atom is a valid id, or false otherwise
  return a.kind == aSymbol and not is_quoted(a)


proc `$`(obj: Atom): string =
  ## Convert ``Atom`` to ``string``
  case obj.kind:
  of aList:                                      # List
    result = "( "
    for i in obj.list: result &= $i & " "
    result &= ")"
  of aNumber:                                    # Number
    let
      f = obj.n.toFloat
      i = obj.n.toInt
    if f - i.float != 0: return $f
    else: return $i
  of aSymbol:                                    # Symbol
    if obj.is_string:
      return obj.str_strip()
    else:
      return obj.s
  of aBool:                                      # Boolean
    return if obj.b: "T" else: "NIL"
  of aFun:                                       # Function 
    if obj.f.body.len > 0:
      let args = if obj.f.args.len > 0:
          "(" & obj.f.args.foldl(a & ", " & b) & ")"
        else:
          "()"
      result = "Function " & args
    else:
      result = "Built-in function"
  of aError:                                     # Error
    return "ERROR: " & obj.e.s 


# NumberBoolFunc Procs #
proc `==`*(a, b: Number): bool =
  return rationals.`==`(a, b)


proc `!=`*(a, b: Number): bool =
  return not rationals.`==`(a, b)


# Error Procs #
proc `$`*(obj: Error): string =
  return "ERROR: " & obj.s


proc write*(err: Error): Error = 
  writeLine(stderr, $err)
  return err


# List procs #
proc reverse*(input: seq[Atom]): seq[Atom] {.noSideEffect.} =
  ## Reverse ``input`` sequence
  result = @[]
  for i in countdown(input.high, 0):
    result.add(input[i])

  
proc cons*(elem, lst: Atom): Atom =
  ## ``cons`` an element to the beginning of a list
  var rev = lst.list.reverse()
  rev.add(elem)
  return atom rev.reverse()


proc snoc*(lst, elem: Atom): Atom =
  ## ``snoc`` an element to the end of a list
  result = atom lst.list
  result.list.add(elem)


proc car*(x: Atom): Atom =
  ## ``Return`` head of ``Atom.list``
  return case x.kind:
  of aList:
    if x.list.len > 0: x.list[0]
    else: atom error "Empty list"
  of aError:
    x
  else:
    atom error "Not a list: " & $x


proc cdr*(x: Atom): Atom =
  ## ``Return`` tail of ``Atom.list``
  return case x.kind:
  of aList:
    if x.list.len > 1: atom(x.list[1..^1])
    elif x.list.len == 1: atom @[]
    else: atom error "Empty list"
  of aError:
    x
  else:
    atom error "Not a list: " & $x


proc quote*(x: Atom): Atom =
  ## ``Return`` tail of ``x.list`` as list or as sigle ``Atom`` (in case of tail's length of `1`)
  return case x.kind:
  of aList:
    if x.list.len != 2:
      atom error "Quote requires 1 argument"
    else:
      x.list[1]
  else:
    atom error "Illegal argument for quote"


proc toStrList*(a: Atom): seq[string] =
  ## Convert ``Atom.list`` to a sequence of strings
  result = @[]
  case a.kind:
    of aList:
      for i in a.list:
        if is_valid_id(i):
          result.add(i.s)
        else:
          return @[]
    else:
      return


proc take*[T](ls: seq[T], n: int = 1): seq[T] =
  return if n > ls.len: ls
         elif n < 1: @[]
         else: ls[0..<n]


proc takelast*[T](ls: seq[T], n: int = 1): seq[T] =
  return if n > ls.len: ls
         elif n < 1: @[]
         else: ls[^n..^1]


proc drop*[T](ls: seq[T], n: int = 1): seq[T] =
  return if n > ls.len: @[]
         elif n < 1: ls
         else: ls[n..ls.high]


proc droplast*[T](ls: seq[T], n: int = 1): seq[T] =
  return if n > ls.len: @[]
         elif n < 1: ls
         else: ls[0..<(ls.len - n)]


# Lambda # 
proc lambda*(args: seq[string], body: seq[Atom], env: Env): Fun =
  return Fun(args: args, body: body, env: env)


# Env #
proc newEnv(pairs: openArray[(string, Atom)], outer: Env = nil): Env =
  ## Create a new ``Env``
  Env(table: newTable[string, Atom](pairs), outer: outer)


proc `[]`(env: Env, key: string): Atom =
  ## Get ``key`` value from environment ``env``
  let key = key.toLower()
  return if env.table.contains(key): env.table[key]
         elif env.outer != nil: env.outer[key]
         else: atom error "No such variable: " & key


# Global Env #
template fun_isType*(typ: AtomKind, args: openArray[Atom]): untyped =
  ## Template for a function of type ``kind(atom): bool`` 
  if args.len < 1:
    return atom error "` or more arguments expected"
  for i in args:
    if i.kind != typ:
      return atom false
  return atom true


template fun_numbers*(op: untyped, args: openArray[Atom]): untyped =
  ## Template for a function of type ``op(num1, num2, ...): num``
  if args.len < 1:
    return atom error "Math functions need at least 1 argument"
  case args[0].kind:
  of aNumber:
    result = atom args[0].n
  of aError:
    return args[0]
  else:
    return atom error "Not a number: " & $args[0]

  for i in 1..args.high:
    case args[i].kind:
    of aNumber:
      try:
        result.n = op(result.n, args[i].n)
      except:
        return atom error getCurrentExceptionMsg()
    of aError:
      return args[i]
    else:
      return atom error "Not a number: " & $args[i]


template fun_bool*(op: untyped, args: openArray[Atom]): untyped =
  ## Template for a function of type ``op(num1, num2, ...): bool``
  if args.len < 2:
    return atom error "Comparison functions need at least 2 arguments"
  result = Atom(kind: aBool)
  if args[0].kind notin {aNumber, aBool}:
    return atom error "Not a number nor bool: " & $args[0]
  
  for i in 1..args.high:
    let
      prev = args[i-1]
      curr = args[i]
    
    case prev.kind:
    of aError:
      return prev  # Error itself

    # Left is a number
    of aNumber:
      case curr.kind:
      of aNumber:
        result.b = op(prev.n, curr.n)
      of aError:
        return curr  # Error itself
      else:
        return atom error "Not a number: " & $curr

    # Left is boolean
    of aBool:
      case curr.kind:
      of aBool:
        result.b = op(prev.b, curr.b)
      of aError:
        return curr  # Error itself
      else:
        return atom error "Not a boolean: " & $curr

    # Have no idea what left is
    else:
      return atom error "Neither a number nor a bool: " & $args[i]


proc fun_isBool*(args: openArray[Atom]): Atom {.cdecl.} =
  fun_isType(aBool, args)


proc fun_isNumber*(args: openArray[Atom]): Atom {.cdecl.} =
  fun_isType(aNumber, args)


proc fun_is_null*(args: openArray[Atom]): Atom {.cdecl.} =
  if args.len != 1:
    return atom error "Null? needs one argument"
  let fst = args[0]
  return case fst.kind:
  of aList:
    atom(fst.list.len <= 0)
  of aBool:
    atom(not fst.b)
  of aError:
    fst  # Returns aError itself
  else:
    atom false


proc fun_is_defined*(args: openArray[Atom]): Atom {.cdecl.} =
  if args.len != 1:
    return atom error "Defined? needs one argument"
  let fst = args[0]
  return case fst.kind:
  of aError:
    atom false
  else:
    atom true


proc fun_plus*(args: openArray[Atom]): Atom {.cdecl.} =
  fun_numbers(`+`, args)


proc fun_minus*(args: openArray[Atom]): Atom {.cdecl.} =
  fun_numbers(`-`, args)


proc fun_multiply*(args: openArray[Atom]): Atom {.cdecl.} =
  fun_numbers(`*`, args)


proc fun_divide*(args: openArray[Atom]): Atom {.cdecl.} =
  fun_numbers(`/`, args)


proc fun_max*(args: openArray[Atom]): Atom {.cdecl.} =
  fun_numbers(`max`, args)


proc fun_min*(args: openArray[Atom]): Atom {.cdecl.} =
  fun_numbers(`min`, args)


proc fun_abs(args: openArray[Atom]): Atom {.cdecl.} =
  if args.len <= 0:
    return atom error "abs needs 1 argument"
  if args[0].kind == aNumber:
    return atom abs(args[0].n)
  elif args[0].kind == aError:
    return args[0]  # return aError itself
  else:
    return atom error "Not a number: " & $args[0]


proc fun_round*(args: openArray[Atom]): Atom {.cdecl.} =
  if args.len <= 0:
    return atom error "round needs 1 argument"
  if args[0].kind == aNumber:
    return atom number((args[0].n).toInt)
  elif args[0].kind == aError:
    return args[0]
  else:
    return atom error "Not a number: " & $args[0]


proc fun_mod*(args: openArray[Atom]): Atom {.cdecl.} =
  if args.len != 2:
    return atom error "Mod needs 2 arguments"
  for i in args:
    case i.kind
    of aNumber: continue
    of aError: return i
    else: return atom error "Not a number: " & $i
  let
    fst = args[0]
    snd = args[1]
    a = fun_divide([fst, snd])
    b = fun_round([a])
  return atom(fst.n - b.n * snd.n)


type NumberBoolFun = proc(a, b: Number): bool
proc odd_or_even*(chk: NumberBoolFun, args: openArray[Atom]): Atom =
  if args.len < 1:
    return atom error "Odd/even functions need at least 1 argument"
  for i in args:
    case i.kind
    of aNumber:
      if chk(fun_mod([i, atom(number(2))]).n, atom(number(0)).n):
        return atom false
    of aError:
      return i
    else:
      return atom error "Not a number: " & $i
  return atom true


proc fun_odd*(args: openArray[Atom]): Atom {.cdecl.} =
  return odd_or_even(`==`, args)


proc fun_even*(args: openArray[Atom]): Atom {.cdecl.} =
  return odd_or_even(`!=`, args)


proc fun_eq*(args: openArray[Atom]): Atom {.cdecl.} =
  result = atom true
  if args.len < 2:
    return atom error "Not enough values to compare"

  for i in 1..args.high:
    if args[i-1] != args[i]:
      return atom false


proc fun_ne*(args: openArray[Atom]): Atom {.cdecl.} =
  not fun_eq(args)


proc fun_gt*(args: openArray[Atom]): Atom {.cdecl.} =
  fun_bool(`>`, args)


proc fun_lt*(args: openArray[Atom]): Atom {.cdecl.} =
  fun_bool(`<`, args)


proc fun_ge*(args: openArray[Atom]): Atom {.cdecl.} =
  fun_bool(`>=`, args)


proc fun_le*(args: openArray[Atom]): Atom {.cdecl.} =
  fun_bool(`<=`, args)


proc fun_cons*(args: openArray[Atom]): Atom {.cdecl.} =
  if args.len != 2:
    return atom error "cons needs 2 arguments: atom and list"
  let
    elem = args[0]
    lst = args[1]
  return case lst.kind
  of aList:
    case elem.kind
    of aSymbol, aNumber, aBool, aFun:
      cons(elem, lst)
    of aError:
      elem
    else:
      atom error "First argument to cons must be either a symbol or value"
  else:
    atom error "Second argument to cons must be a list"


proc fun_snoc*(args: openArray[Atom]): Atom {.cdecl.} =
  if args.len != 2:
    return atom error "snoc needs 2 arguments: list and atom"
  let
    lst = args[0]
    elem = args[1]
  return case lst.kind
  of aList:
    case elem.kind
    of aSymbol, aNumber, aBool, aFun:
      snoc(lst, elem)
    of aError:
      elem
    else:
      atom error "Second argument to snoc must be a symbol or a value"
  else:
    atom error "First argument to snoc must be a list"


proc fun_car*(args: openArray[Atom]): Atom {.cdecl.} =
  if args.len != 1:
    return atom error "car needs 1 argument"
  return case args[0].kind
  of aList:
    car atom args[0].list
  of aError:
    args[0]
  else:
    atom error "car argument must be of list type"


proc fun_cdr*(args: openArray[Atom]): Atom {.cdecl.} =
  if args.len != 1:
    return atom error "cdr needs 1 argument"
  return case args[0].kind
  of aList:
    cdr atom args[0].list
  of aError:
    args[0]
  else:
    atom error "cdr argument must be of list type"


proc fun_len*(args: openArray[Atom]): Atom {.cdecl.} =
  if args.len != 1:
    return atom error "len needs 1 argument"
  let fst = args[0]
  if fst.kind == aError:
    return fst
  elif fst.kind == aList:
    return atom number(len(args[0].list))
  elif fst.is_string:
    return atom number fst.str_strip.runeLen
  else:
    return atom error "Neither list nor string: " & $fst


proc fun_echo*(args: openArray[Atom]): Atom {.cdecl.} =
  if args.len != 1:
    return atom error "echo needs 1 argument"
  let fst = args[0]
  echo $fst
  return fst


proc fun_repr(args: openArray[Atom]): Atom {.cdecl.} =
  if args.len != 1:
    return atom error "repr needs 1 argument"
  let fst = args[0]
  echo $fst.repr
  return fst


proc fun_print*(args: openArray[Atom]): Atom {.cdecl.} =
  if args.len < 1:
    return atom error "print needs 1 or more arguments"
  for arg in args:
    stdout.write $arg
  stdout.write "\n"
  return atom()
