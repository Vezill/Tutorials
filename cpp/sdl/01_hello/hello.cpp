#include "hello.hpp"


int main(int argc, char* argv[])
{
    // The window to render to
    SDL_Window* window = NULL;

    // The surface contained by the window
    SDL_Surface* screenSurface = NULL;

    // Init SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) 
    {
        std::printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
    }
    else 
    {
        // Create window
        window = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                  SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
    }
}
