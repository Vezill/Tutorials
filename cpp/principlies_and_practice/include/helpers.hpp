#ifndef _HELPERS_HPP_
#define _HELPERS_HPP_

#include <iostream>


inline void keep_window_open()
{
	std::cin.clear();
	std::cout << "Please enter a character to exit\n";
	char ch;
	std::cin >> ch;
	return;
}

#endif // _HELPERS_HPP_
